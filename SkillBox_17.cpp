

#include <iostream>
#include <cmath> 

class Vector
{
public:
    Vector(double _x,double _y,double _z)
        : x(_x),y(_y),z(_z)
    {

    }
    
    double VectorModul(double _x, double _y, double _z)
    {
        double modul = sqrt(_x * _x + _y * _y + _z * _z);
        return modul;
    }

    
    void ShowVector()
    {
        std::cout << x << "\t" << y << "\t" << z << "\n";
    }



private:
    double x;
    double y;
    double z;
};


int main()
{
    Vector v(1,2,3);
    v.ShowVector();
    std::cout << v.VectorModul(2,2,2) << std::endl;

    return 0;
}